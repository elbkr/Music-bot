# Discord music bot

## How to make it work?

- Clone or download this repository
- Enter code editor
- Run `npm install` on the console
- Go to config.json and edit:
> `PREFIX` with a prefix of your choice
> 
> `TOKEN` with your bot's token
> 
> `YOUTUBE_API_KEY` With your youtube API key
> 
> `QUEUE_LIMIT` With the max numbers of songs in queue at the same time
> 
> `COLOR` With the color you want for the embeds 
> 
> `inviteURL` With the invite link of your bot

- Run the bot and all done!

## How to get a YouTube API key?

- Go to https://console.developers.google.com/
- Login or register
- Click on `Create project`
- Go to `Libary` and select **Youtube Data API**
- Click on `enable`
- Then go to credentials (left tab) and click on **Create credentials** > **API key**
- Copy your key and paste it on `config.json`
